IT2810 Web Development - Exercise 3.3
======
### ChartCinema - An overview of your favorite films, including graphs through space and time.

**** THIS README IS DEPRECATED ****


# Get started
1. Clone the project
`> git clone https://paulpmit@bitbucket.org/trondaal/it2810-22-oppgave-3.git `

2. Navigate into that directory
`> cd it2810-22-oppgave-3/`

## Running the Angular application
1. Navigate to Angular directory, install dependencies and run project
```
> cd angular/
> npm install
> npm start
```

2. Open a web browser and navigate to `localhost:3000`

## Running the React application
1. Navigate to the React directory, install dependencies and run project
```
> cd react/
> npm install
> npm start
```

2. Open a web browser and navigate to `localhost:8888`
3. You can also build a production ready bundle by running `> npm run build` in the React directory - this will minify and optimize the resulting bundle file, and place it it `/dist`.

# Technology choices
* Webpack (React)
* SystemJS (Angular)
* Sass
* RxJS (Angular) 

# File Structures
In both the Angular and React applications the project files are structured by domain. This is to keep files related to the same functionality close together. This helps making changes to functionality easier, as all files related to some functionality/domain are kept together in separate folders.

## Angular


```
app
|-- app.component.ts|css
|-- app.module.ts
|-- config.ts
|-- main.ts
|
|-- lists
|   |-- movie-detail.component.ts|html|css
|   |-- movie-master.component.ts|html|css
|    
|-- media
|   |-- media-search.component.ts|html|css
|   |-- media-search.service.ts
|   |-- media.service.ts
|   |-- movie.model.ts
|
|-- navbar
|   |-- navbar.component.ts|html
|
|-- utility
    |-- app-routing.module.ts
    |-- rxjs-extensions.ts

```

#### app/
This directory contains the root Angular component with the <my-app> directive. In addition, we define an Angular module which acts as a barrel for all other modules and components which we use throughout the app. This root module is then bootstrapped in main.ts, which is the entry point of our SystemJS configuration. The config.ts file simply holds constants for API endpoints and keys.

#### /lists
Holds all list-related components. We use a Master/Detail architecture, and currently this directory only contains movie components (MovieMaster and MovieDetail). In the future, we will also add a MovieItem component, as well as corresponding TV Show components.

#### /media
Directory for media-related components, services and models. There's a MediaSearchComponent which uses the MediaSearchService to get results from HTTP GETs to the /movie/search endpoint of TMDB. The MediaSearchService utilizes observables in order to be able to cancel requests and handle debouncing, in an effort to reduce total API calls. The MediaService is responsible for handling HTTP requests to the /movie, /discover and /tv endpoints so that the results are presentable in a grid.

#### /navbar
This directory is due for changes in upcoming versions, one of which is to change the directory name to 'navigation', so that we can add a Sidebar-, and NotFound-component.

#### /utility
This folder is meant to hold utility scripts. As of now, we configure the application routes in an AppRoutingModule. The rxjs-extensions file is simply a barrel for specific RxJS module imports.



## React
```
src
|-- App.jsx
|-- index.jsx
|-- index.scss
|-- template.html
|
|-- Movies
|   |-- MovieContainer.jsx
|   |-- MovieItem.jsx
|   |-- MovieList.jsx
|    
|-- Navigation
|   |-- NavigationBar.jsx
|   |-- NotFound.jsx

```
#### Movies
Directory for movie-related functionality, currently movie list and movie items, including functionality for making API-calls to the moviedb-API.
#### Navigation
This domain currently contains a NavigationBar-component and a NotFound-component. These components are tied to the navigation of the site.
