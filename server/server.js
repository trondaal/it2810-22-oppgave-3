var express  = require('express');
var app      = express();                             
var bodyParser = require('body-parser');

var User = require('./model/user');

//app.use(express.static(path.join(__dirname, 'model')));
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());  

var router = express.Router();

router.route('/users')
    .post(function(req, res) {
        var userId = req.body.user_id

        User.find({fbId : userId}, function (err, user) {
            if(err) { throw err; }
            if(user){ res.status(409).json({message: "user exists."}); }
            else {
                var newUser = new User();
                newUser.fbId = userId;
                newUser.name = req.body.name;
                newUser.watchlist = []
                newUser.save(function(err) {
                    if (err) res.send(err);
                    res.status(201).json({message: "User added."})
                });
            }
        });
    })

    .get(function(req, res) {
        User.find(function(err, users) {
            if (err) res.send(err);
            res.status(200).json(users)
        })
    })

router.route('/users/:id')
    .get(function(req, res) {
        var userId = req.params.id;
        User.find({fbId : userId}, function(err, user) {
            if (err) res.send(err);
            res.status(200).json(user);
        });
    })

router.get('/users/:id/watchlist', function(req, res) {

    var userId = req.params.id;

    User.find({fbId : userId}, function(err, user) {
        if (err) res.send(err);
        if(!user) {
            res.status(404).json({message: "User not found."});
        } else {
            res.status(200).json(user.watchList);
        }
    });
});

router.post('users/:userid/watchlist', function (req, res) {
    var userId = req.params.userid;
    var movie = req.body

    User.find({fbId : userId}, function(err, user) {
        if(err) res.send(err);
        if(!user){
            res.status(404).json({message: "Invalid user."});
        } else {
            user.watchList.push(movie);
            user.save(function(err) {
                if (err) res.send(err);
                res.status(201).json({message: "Movie added to watchlist."})
            })
        }
    });
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/api', router)


app.listen(process.env.PORT || 3000, function () {
  console.log('Listening on port 3000!');
});