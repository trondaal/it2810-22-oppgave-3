var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var movieSchema = new Schema({
    poster_path: String,
    adult: Boolean,
    overview: String,
    release_date: String,
    genre_ids: [Number],
    id: Number,
    original_title: String,
    original_language: String,
    title: String,
    backdrop_path: String,
    popularity: Number,
    vote_count: Number,
    video: Boolean,
    vote_average: Number
});

mongoose.connect('mongodb://localhost:27017/');
module.exports = mongoose.model('Movie', movieSchema);